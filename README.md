#TP MMI2 : mise en application jQuery
> **Avant de commencer :**
> - Curriculum est la page chargé par défaut pour ce TP : ```loadPage('curriculum');```
> - Pour sortir un item au hasard d'un tableau, un helper random() à était ajouté dans all.js
> - Des exemples d'usage l'accompagnent.
> - Excepté la première consigne, les autres sont à rédiger dans la fonction nommée **tp3**.
----------

### **TP 3** : 

- Indentez proprement all.js
- Ajoutez un astérisque (*) en fin de texte, et une esperluète (&) en début de texte pour chaque lien contenu dans ```<article>```.
- Supprimer le deuxième lien contenu dans ```<article>```.
- Au click sur un titre, changez le texte de couleur au hasard parmi 3 couleurs. Pour cela, vous devrez créer en amont une variable de type "tableau" en JavaScript contenant ces trois couleurs.
- **En une seule instruction jQuery** : sélectionnez parmi les ```<li>``` celui contenant une balise ```<strong>``` puis déplacer celui-ci en première position dans la liste. Enfin, changez sa couleur de texte à "red". 
- **En une seule instruction jQuery** : clonez ce ```<li>```, placez-le à la position médiane (au milieu) et changer la couleur du texte en "blue".
- **En une seule instruction jQuery** : supprimez chaque ```<li>``` placé **après** un ```<li>``` contenant une balise ```<strong>```. Puis changer la couleur du texte des ```<li>``` placé **avant** un ```<li>``` contenant une balise ```<strong>``` à "green".
- **En une seule instruction jQuery** : supprimez-les ```<li>``` dont le texte n'est pas coloré.
- Créez un paragraphe contenant l'ensemble des textes de chaque ```<li>```, juste après la liste.

### **Méthodes jQuery** :

**next()** // **end()** // **prev()** // **after()** // **find()** // **each(function(){...)** // **parent()** // **prependTo()** // **css()** // **clone()** // **remove()** // **insertAfter()** // **eq()** // **not()** // **on()** // **eq()**

### **Fonction JavaScript natives** :

**Math.floor()** // **console.log()**

