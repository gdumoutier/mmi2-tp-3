<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>MMI2 TP1</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto+Mono" rel="stylesheet">
</head>
<body>
<div class="layout remodal-bg">
	<header role="banner">
		<div class="wrapper" data-relative-input="true" id="scene">
			<p class="logo" data-depth="0.6">
				<a href="#" data-page="accueil">
					Mon portfolio
				</a>
			</p>
		</div>
		<nav role="navigation">
			<div class="wrapper">
				<ul>
					<li><a href="index.php" data-page="accueil"><i class="fa fa-fw fa-home"></i> Accueil</a></li>
					<li><a href="#" data-page="galerie">Galerie</a></li>
					<li><a href="#" data-page="curriculum"><i class="fa fa-fw fa-graduation-cap"></i> Curriculum</a></li>
					<li><a href="#" data-remodal-target="contact"><i class="fa fa-fw fa-envelope"></i> Contact</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<main>
		<div class="wrapper">
			<div id="pages"></div>
		</div>
	</main>
	<footer>
		<div class="wrapper">
			<ul>
				<li class="social"><a href="//twitter.com"><i class="fa fa-fw fa-twitter"></i></a></li>
				<li class="social"><a href="//twitter.com"><i class="fa fa-fw fa-facebook"></i></a></li>
			</ul>
		</div>
	</footer>
</div>

<div class="remodal" data-remodal-id="contact">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Contact</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ipsam itaque <a href="">minima</a>, nesciunt numquam, odio officia quia quo quod recusandae tempore totam voluptatem, voluptatum. Minima molestias nemo nihil perferendis ullam.</p>
	<br>
	<form action="">
		<div class="form-group"><label for="name">Nom</label><input id="name" name="name" type="text" class="form-control"></div>
		<div class="form-group"><label for="mail">Email</label><input id="mail" name="mail" type="text" class="form-control"></div>
		<div class="form-group"><label for="message">Votre message</label><textarea name="message" id="message" cols="20" rows="5" class="form-control"></textarea></div>
	</form>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">Envoyer</button>
</div>

<script src="https://use.fontawesome.com/601a2e39a6.js"></script>
<script src="js/jquery.js"></script>
<script src="js/autosize.js"></script>
<script src="js/jquery.owl.carousel.js"></script>
<script src="js/jquery.lightgallery-all.js"></script>
<script src="js/lazysizes.min.js"></script>
<script src="js/remodal.js"></script>
<script src="js/all.js"></script>

</body>
</html>