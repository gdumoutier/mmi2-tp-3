(function ($) {

var insGallery, gallery, galleryOptions = {thumbnail: true, animateThumb: true, showThumbByDefault           : false,
download                     : false,
selector                     : '.unfiltered a'
}
;

/*==========================================================*/

function init_galerie() {
if (insGallery) {
insGallery.destroy(true);
}
gallery = $('.galerie');
gallery.lightGallery(galleryOptions);
insGallery = gallery.first().data('lightGallery');
}

/*==========================================================*/

function init_carousel() {
$('.slider').owlCarousel({
items: 1
}
);
}

init_carousel();

/*==========================================================*/

autosize(
$('textarea'));

/*==========================================================*/

function loadPage(target) {
$('nav a.active').removeClass('active');
$('[data-page="' + target + '"]')
.addClass('active');
$.get("pages/" + target + ".html",
function (data) {
$("#pages").html(data);
init_carousel();
init_galerie(); tp3();
window.lazySizesConfig.lazyClass = 'lazyload';
}
);
}

$('a[data-page]')
.click(function (event) {
event.preventDefault();
var page = $(this).attr('data-page');
if (page != "undefined") {
loadPage(page);
}
}
);

loadPage('curriculum');

/*=========================================\
$HELPER RANDOM
\=========================================*/

Array.prototype.random = function () {
return this[Math.floor((Math.random()*this.length))];
};

// [2,3,5].random()

//list = [2,3,5]
// list.random()

/*=========================================\
$BONUS
\=========================================*/

function tp3(){

}

})(jQuery);
